@extends('layouts.app')

@section('content')

        <div id="task6" class="task-div">
            <h2>Задача 6</h2>
            <div class="p-50">
                <p class="task-t">
                    Чем отличается оператор == от === ?<br>
                    == Перед сравнением оператор равенства приводит обе величины к общему типу.<br>
                    === Строгое равно проверяет на равенство две величины, при этом тип каждой из величин перед сравнением не изменяется (не приводится). Если значения имеют различающиеся типы, то они не могут быть равными. С другой стороны все не числовые переменные, принадлежащие одному типу, считаются равными между собой, если содержат одинаковые величины.
                </p>
                <code class="ex-code">
                    var num = 0;<br>
                    var obj = new String("0");<br>
                    var str = "0";<br>
                    var b = false;<br>

                    console.log(num == num); // true<br>
                    console.log(obj == obj); // true<br>
                    console.log(str == str); // true<br>

                    console.log(num == obj); // true<br>
                    console.log(num == str); // true<br>
                    console.log(obj == str); // true<br>
                    console.log(null == undefined); // true<br>

                    console.log(num === num); // true<br>
                    console.log(obj === obj); // true<br>
                    console.log(str === str); // true<br>

                    console.log(num === obj); // false<br>
                    console.log(num === str); // false<br>
                    console.log(obj === str); // false<br>
                    console.log(null === undefined); // false<br>
                </code>
                <div class="result">
                    <script type="text/javascript">
                        var num = 0;
                        var obj = new String("0");
                        var str = "0";
                        var b = false;

                        console.log(num == num); // true
                        console.log(obj == obj); // true
                        console.log(str == str); // true

                        console.log(num == obj); // true
                        console.log(num == str); // true
                        console.log(obj == str); // true
                        console.log(null == undefined); // true

                        console.log(num === num); // true
                        console.log(obj === obj); // true
                        console.log(str === str); // true

                        console.log(num === obj); // false
                        console.log(num === str); // false
                        console.log(obj === str); // false
                        console.log(null === undefined); // false
                    </script>
                    <p>Результат выполнения кода в консоли.</p>
                </div>
            </div>
        </div>
@endsection