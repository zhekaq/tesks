@extends('layouts.app')

@section('content')

        <div id="task8" class="task-div">
            <h2>Задача 8</h2>
            <div class="p-50">
                <p class="task-t">
                    Какие данные пользователя сайта из перечисленных можно считать на 100%
                    достоверными: cookie, данные сессии, IP-адрес пользователя , User-Agent? Почему?
                </p>
                <code class="ex-code">
                </code>
                <div class="result">
                    Данные сесии так как реализуеться исключительно на стороне сервера.
                    Все остальное храняться не на стороне сервера поетому их нельзя считать достоверными на 100%.
                </div>
            </div>
        </div>
@endsection