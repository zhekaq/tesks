<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/task', function () {
    return view('welcome');
});
Route::get('/task/1', 'TaskController@TaskOne');
Route::get('/task/2', 'TaskController@TaskTwo');
Route::get('/task/3', 'TaskController@TaskThree');
Route::get('/task/4', 'TaskController@TaskFour');
Route::get('/task/5', 'TaskController@TaskFive');
Route::get('/task/6', 'TaskController@TaskSix');
Route::get('/task/7', 'TaskController@TaskSeven');
Route::get('/task/8', 'TaskController@TaskEight');
Route::get('/task/9', 'TaskController@TaskNine');
Route::get('/task/10', 'TaskController@TaskTen');