<?php
namespace App\Repositories;

use App\Events;

class EventsRepository
{
    public function all()
    {
        return Events::orderBy('id', 'asc')
            ->get();
    }

}