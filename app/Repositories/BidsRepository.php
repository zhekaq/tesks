<?php
namespace App\Repositories;

use App\Bids;

class BidsRepository
{
    public function all()
    {
        return Events::orderBy('id', 'asc')
            ->get();
    }
    
}