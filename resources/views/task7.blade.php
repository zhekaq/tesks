@extends('layouts.app')

@section('content')

        <div id="task7" class="task-div">
            <h2>Задача 7</h2>
            <div class="p-50">
                <p class="task-t">
                    Чем отличается require от include ?
                </p>
                <code class="ex-code">

                </code>
                <div class="result">
                    <p>
                        Отличия между require() и include() таковы, что require() возвращает FATAL ERROR, если файл не найден, include() же возвращает только WARNING.<br>
                    Допустим подключаем файл с помощью require("some_file.txt"); перед подключением этого файла функция проверет его наличие и в случае его отсутствия вернет ошибку.
                        А при использовании include("some_file.txt"); функция сразу попытаеться его подключить и в случае отсутствия файла выдаст предупреждение.
                    </p>
                </div>

            </div>
        </div>
@endsection