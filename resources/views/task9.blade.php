@extends('layouts.app')

@section('content')

        <div id="task9  " class="task-div">
            <h2>Задача 9</h2>
            <div class="p-50">
                <p class="task-t">
                    Что выведет следующий код на JavaScript и почему:<br>
                    for( var i =0; i < 10; i++){<br>
                    setTimeout(function () {<br>
                    console.log(i);<br>
                    }, 100);<br>
                    }
                </p>
                <code class="ex-code">
                    for( var i =0; i < 10; i++){
                    setTimeout(function () {
                    console.log(i);
                    }, 100);
                    }
                </code>
                <div class="result">
                    <br>
                    <p>
                        Следующий код выведет 10 раз значение 10. Так как функция setTimeout будет выполняться по завершению цикла. После завершения цикла значение i = 10.
                    </p>
                    <script type="text/javascript">
                        for( var i =0; i < 10; i++){
                            setTimeout(function () {
                                console.log(i);
                            }, 100);
                        }
                    </script>
                </div>
            </div>
        </div>
@endsection