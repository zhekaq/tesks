@extends('layouts.app')

@section('content')

        <div id="task2" class="task-div">
            <h2>Задача 2</h2>
            <div class="p-50">
                <p class="task-t">
                    Реализовать алгоритм извлечения числовых значений со строки:
                    <strong>This server has 386 GB RAM and 5000 GB storage</strong>
                </p>
                <code class="ex-code">
                    $pattern = "/[0-9]+/";<br>
                    $input_str = "This server has 386 GB RAM and 5000 GB storage";<br>
                    preg_match_all($pattern, $input_str, $matches_out);
                </code>
                <div class="result">
                    <p>Числовые значения</p>
                    @foreach($task as $mached)
                        {{$mached}}
                        <br>
                    @endforeach

                </div>
            </div>
        </div>

@endsection