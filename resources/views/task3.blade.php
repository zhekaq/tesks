@extends('layouts.app')

@section('content')

        <div id="task3" class="task-div">
            <h2>Задача 3</h2>
            <div class="p-50">
                <p class="task-t">
                    ​Как получить первый элемент массива ​[2,3,56,65,56,44,34,45,3,5,35,345,3,5] ​?
                </p>
                <code class="ex-code">
                    $array =[2,3,56,65,56,44,34,45,3,5,35,345,3,5];<br>
                    $first = array_first($array);
                </code>
                <div class="result">
                    <p>Первый елемент {{$task}} </p>
                </div>
            </div>
        </div>

@endsection