<?php use Illuminate\Http\Request;?>
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">

    <div class="content">
        <div class="title ">
            Задачи

        </div>
        <?php
        $curr = Request::capture()->getRequestUri();
        ?>
        <div class="links">
            <?php for($i=1;$i<11;$i++):?>
            <a href="/task/<?php echo $i;?>"
            <?php
                if("/task/".$i==$curr){echo "class=\"active\"";}
                ?>
            >Задача <?php echo $i;?></a>
           <?php endfor?>
        </div>

    </div>
</div>

@yield('content')

</body>
</html>