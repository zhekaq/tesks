@extends('layouts.app')

@section('content')
        <div id="task10  " class="task-div">
            <h2>Задача 10</h2>
            <div class="p-50">
                <p class="task-t">
                    В базе данных хранится список мероприятий (таблица events) и список заявок на
                    покупку билетов на указанные мероприятия (таблица bids).
                </p>
                <p>
                    1. Сделать миграцию для выше указанной схемы и залить в базу тестовые данные<br>
                    2. Напишите запрос, который выберет имя пользователя (bids.name) с самой<br>
                    высокой ценой заявки (bids.price)<br>
                    3. Напишите запрос, который выберет название мероприятия (events.caption), по<br>
                    которому нет заявок<br>
                    4. Напишите запрос, который выберет название мероприятия (events.caption), по<br>
                    которому больше трех заявок<br>
                    5. Напишите запрос, который выберет название мероприятия (events.caption), по<br>
                    которому больше всего заявок
                </p>

                <div class="result">
                    <br>
                    <p>
                        1. Файлы находятся в соответствующих папках.
                    </p>
                    <code class="ex-code">
                        2.Запрос:<br>
                        $query = DB::select('SELECT bids.name,bids.price FROM bids WHERE bids.price = (SELECT MAX(bids.price) FROM bids ) ');<br>
                    </code>
                    <p>
                        2.Результат:<br>
                        Имя {{$task[0][0]->name}}
                    </p>
                    <code class="ex-code">
                        3.Запрос:<br>
                        $query = DB::select('SELECT events.caption,events.id FROM events WHERE events.id NOT IN (SELECT bids.id_event FROM bids ) ');<br>
                    </code>
                    <p>
                        3.Результат:
                        <br><strong>Название мероприятия</strong> <br>
                        <?php $i=0; ?>
                        @foreach($task[1] as $curName)
                            <?php echo ++$i." ";?>{{$curName->caption}} <br>
                        @endforeach
                    </p>
                    <code class="ex-code">
                        4.Запрос:<br>
                        $query = DB::select('SELECT caption,id FROM events WHERE id IN (SELECT id_event AS ln FROM (SELECT id_event, count(*) as Counter FROM bids GROUP BY id_event) AS tbl WHERE Counter > 3)');<br>
                    </code>
                    <p>
                        <strong>4.Результат:<br>Название мероприятия</strong> <br>
                        <?php $i=0; ?>
                        @foreach($task[2] as $curName)
                            <?php echo ++$i." ";?>{{$curName->caption}} <br>
                        @endforeach
                    </p>
                    <code class="ex-code">
                        5.Запрос:<br>
                        $query = DB::select('SELECT caption, id FROM events WHERE id IN (SELECT MAX(Counter) AS ln FROM (SELECT id_event, count(*) as Counter FROM bids GROUP BY id_event) AS tbl )');
                    </code>
                    <p>
                        <strong>5.Результат:<br>Название мероприятия</strong> <br>
                        {{$task[3][0]->caption}}
                    </p>
                </div>
            </div>
        </div>
@endsection