@extends('layouts.app')

@section('content')

        <div id="task4" class="task-div">
            <h2>Задача 4</h2>
            <div class="p-50">
                <p class="task-t">
                    Как вычислить остаток от деления 10/3
                </p>
                <code class="ex-code">
                    $result = 10%3;
                </code>
                <?php
                $result = 10%3;
                ?>
                <div class="result">
                    <p>Результат {{$task}}</p>
                </div>
            </div>
        </div>

@endsection