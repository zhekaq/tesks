<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\TasksRepository;

class TaskController extends Controller
{
    protected $task;

    public function TaskOne()
    {
        $all =28;
        $sport = ceil(28*0.75);
        $result =[$all,$sport];

        return view('task1', [
            'task' => $result,
        ]);
    }

    public function TaskTwo()
    {
        $pattern = "/[0-9]+/";
        $input_str = "This server has 386 GB RAM and 5000 GB storage";
        preg_match_all($pattern, $input_str, $matches_out);
        $result =$matches_out[0];

        return view('task2', [
            'task' => $result,
        ]);

    }
    public function TaskThree()
    {
        $array =[2,3,56,65,56,44,34,45,3,5,35,345,3,5];
        $result =array_first($array);

        return view('task3', [
            'task' => $result,
        ]);
    }
    public function TaskFour()
    {

        $result = 10%3;

        return view('task4', [
            'task' => $result,
        ]);
    }
    public function TaskFive()
    {
        $a = [1,2,3,4,5];
        $b = "Hello world";
        $a=[$a,$b];
        $b=$a[0];
        $a=$a[1];
        $result =[$a,$b];

        return view('task5', [
            'task' => $result,
        ]);
    }

    public function TaskSix()
    {
        return view('task6');
    }
    public function TaskSeven()
    {
        return view('task7');
    }
    public function TaskEight()
    {
        return view('task8');
    }
    public function TaskNine()
    {
        return view('task9');
    }
    public function TaskTen()
    {
        $query = DB::select('SELECT bids.name,bids.price FROM bids WHERE bids.price = (SELECT MAX(bids.price) FROM bids ) ');
        $result[]=$query;

        $query = DB::select('SELECT events.caption,events.id FROM events WHERE events.id NOT IN (SELECT bids.id_event FROM bids ) ');
        $result[]=$query;

        $query = DB::select('SELECT caption,id FROM events WHERE id IN (SELECT id_event AS ln FROM (SELECT id_event, count(*) as Counter FROM bids GROUP BY id_event) AS tbl WHERE Counter > 3)');
        $result[]=$query;

        $query = DB::select('SELECT caption, id FROM events WHERE id IN (SELECT MAX(Counter) AS ln FROM (SELECT id_event, count(*) as Counter FROM bids GROUP BY id_event) AS tbl )');
        $result[]=$query;
        return view('task10', [
            'task' => $result,
        ]);
    }
}
