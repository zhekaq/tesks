@extends('layouts.app')

@section('content')

        <div id="task5" class="task-div">
            <h2>Задача 5</h2>
            <div class="p-50">
                <p class="task-t">
                    Нужно поменять 2 переменные местами без использования третьей:<br>
                    $а = [1,2,3,4,5];<br>
                    $b = ‘Hello world’;
                </p>
                <code class="ex-code">
                    $a = [1,2,3,4,5];<br>
                    $b = "Hello world";<br>
                    $a=[$a,$b];<br>
                    $b=$a[0];<br>
                    $a=$a[1];
                </code>
                <div class="result">
                    <p>a = {{$task[0]}}<br>
                       b = <?php print_r($task[1]);?></p>
                </div>
            </div>
        </div>
@endsection