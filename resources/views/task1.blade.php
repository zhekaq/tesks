@extends('layouts.app')

@section('content')

        <div id="task1" class="task-div">
            <h2>Задача 1</h2>
            <div class="p-50">
            <p class="task-t">
                В классе 28 учеников. 75% из них занимаются спортом. Сколько учеников в классе
                занимаются спортом и сколько всего учеников в классе?
            </p>
            <code class="ex-code">
                $all =28;
                $sport = ceil(28*0.75);
            </code>
            <div class="result">
                <p>Всего учеников <?php echo $task[0];?><br>
                Учеников занимается спортом <?php echo $task[1];?></p>
            </div>
            </div>
        </div>

@endsection